This is my Ludum Dare #25 entry. The theme was "You are the villain"


This is seriously the worst code I've ever written, I advise not using
it **ever**, but everything (code/"art"/sound) is licensed under
WTFPL.

Below is the original idea I drafted Friday night. I scaled it down
and changed some things to finish everything.


Clean Energy Tycoon
======================

Okay, so it's called "Clean Energy Tycoon" or something like that. In
the most tongue in cheek way possible.

Basically, you are running this massive energy corporation, named
whatever, and you have to spawn as many oil rigs, offshore platforms,
drills, and nuclear power plants as you can without having the locals
overthrow your destructive facilities.


To appease the locals, you have to balance building these facilities
with spending money on propaganda (ad campaigns, whatever) and buying
legislation that allows you to continue destroying the planet.

Map is random whateverland, doesn't really have to represent anything
real. High and low concentration areas of oil and
resources. Randomized, I guess, if there's time.

Now, what you do is either:

* Blindly build a facility, which leaves you with a potentially
  unusable area.
* Send a probing mission, which is much cheaper and lets you know if
  there is oil or whatever in the immediate area (say some large radius around
  probe, 100 miles or whatever), but can rile up locals (this is kind
  of odd, though, isn't it? I mean, why wouldn't the rig itself stir
  up commotion?)
* Build nuclear plants, which are super expensive, and make the locals
  hostile to any further developments in the area. Or something. Make
  it the highest risk/reward option.

You must connect these through pipelines/supply lines/power
lines/whatever to one of your processing facilities.

Rigs/Pipes leak and pollute the environment. The more valuable the
rig/pipe, the more it leaks.

As pollution grows, public opinion of that rig declines. When certain
threshold is met, rig goes offline due to public protests. At this
point, options are:

* Start social media campaign
* Buy legislation
* Create actual clean energy stuff, which is entirely useless. It
  costs you money, and doesn't do anything at all

Oh also. To build anything, you first need to annihilate the ecosystem
there. Torch forests and fill swamps and whatever else. Basically make
them as evil as possible

So you are the CEO of this conglomerate.

Also something has to happen with goats. And the name "Antigoat
Consortium" must appear somewhere in the game.

Win condition:

* Successfully pipe out all of the oil, coal, whatever

Lose condition:

* Bankrupt
* Clean energy bill is now law.
